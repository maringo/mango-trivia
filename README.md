# Mango Trivia

Mango Trivia benodigdheden:
1. Docker ( https://docs.docker.com/docker-for-windows/install/ )
2. Meteor ( https://www.meteor.com/install )

Mango Trivia opstarten:

1. Open je console en navigeer naar de rootmap van het project
2. typ 'meteor npm install' -- meteor npm gaat nu alle benodigdheden installeren
3. typ 'meteor npm run start' -- meteor start nu Mango Trivia op op localhost:3007
4. importeer exportdb.json in de MongoDB van Docker (met MongoDB Compass bv.) -- de database draait op localhost:3008, zonder credentials (want local).
5. navigeer in de browser naar localhost:3007 en enjoy!

Waar liep ik tegenaan:
Ik krijg het niet voor elkaar om het gekozen antwoord in het systeem te krijgen en bijvoorbeeld een andere kleur mee te geven. Omdat ik hier op vast liep, kreeg ik het ook niet voor elkaar om het scoresysteem uit te werken. De clickhandle terug zetten naar een parentcomponent wilde me niet lukken op de manier dat tijdens het aftellen van de vraag al duidelijk was wel antwoord gekozen was. In de overgang naar de volgende vraag wel.