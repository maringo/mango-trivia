import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { withTracker } from 'meteor/react-meteor-data';
import { Questions } from '../../api/questions.js';


const TIMER_IN_SECONDS = Meteor.settings.public.timerseconds;
const COUNTDOWN_IN_SECONDS = Meteor.settings.public.countdownseconds;
const TOTAL_QUESTIONS = Meteor.settings.public.totalquestions;

/**
 * The QuestionContainer to begin with.
 */
class QuestionContainer extends Component {

	constructor(props) {
		super(props);
		this.state = {
			status: this.props.status,
		};
		this.chosenanswer = '';
	}

	// Will trigger for new question.
	componentWillReceiveProps(props) {
		var question = props.questions[props.question_count-1].Question;
		const correct = props.questions[props.question_count-1].CorrectAnswer;
		const other = props.questions[props.question_count-1].OtherAnswers;
		var tempanswers = other;
		tempanswers.push(correct);
		var shuffle = require('shuffle-array');
		var answers = shuffle(tempanswers);		
		this.setState({
			question_count: props.question_count,
			question: question,
			answers: answers,
			correct: correct
		});
		console.log(this.state.question_count);
	}

	// when chosen an answer, this func will be executed
	handleAnswer(answer){
		this.chosenanswer = answer;
	}

  	render() {
  		if(this.props.status == 'QUESTION_ACTIVE'){
	    	return (
				<div>
					<div className="questioncounter">Question {this.state.question_count} / {TOTAL_QUESTIONS}</div>
					<div className="question">{this.state.question}</div>
					<div className="answers">
			       	{this.state.answers.map((answer, index) => {
			       		var extraclass = '';
			       		if(this.state.chosenanswer == answer){
			       			extraclass = "chosen";
			       		}
			            return <div className={extraclass} onClick={() => { this.handleAnswer({answer}) }}  key={ index }>{answer}</div>;
					})}
					</div>
				</div>
	  		)
	    }
	    else{
		    return (
		    	<div className="waitwrap">
		    	Wait for the next question...
		    	</div>
		    )
		}
  	}
}


export default withTracker(() => {
  return {
    questions: Questions.find({}).fetch(),
  };
})(QuestionContainer);