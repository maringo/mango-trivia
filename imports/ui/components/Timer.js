import React, { Component } from 'react';

/**
 * The startbutton to begin with.
 */
export default class Timer extends Component {

  render() {
    return (
		<div className="timer">
			{this.props.seconds}
		</div>
  	)
  }
}