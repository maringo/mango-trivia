import React, { Component } from 'react';
// Task component - represents a single todo item
export default class Question extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
    	<div>
	       	{this.props.answers.map((answer, index) => {
	            return <div onClick={() => { this.props.handleAnswer({answer}) }}  key={ index }>{answer}</div>;
			})}
      	</div>
    );
  }
}