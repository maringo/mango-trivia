import React, { Component } from 'react';

/**
 * The startbutton to begin with.
 */
export default class StartButton extends Component {

  render() {
    return (
			<button className="btn btn-lg btn-success btn-start" onClick={this.props.onClick}>
				Start!
			</button>
  	)
  }
}