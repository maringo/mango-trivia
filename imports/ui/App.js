import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { withTracker } from 'meteor/react-meteor-data';
import StartButton from '../ui/components/StartButton.js';
import Timer from '../ui/components/Timer.js';
import QuestionContainer from '../ui/components/QuestionContainer.js';


const TIMER_IN_SECONDS = Meteor.settings.public.timerseconds;
const COUNTDOWN_IN_SECONDS = Meteor.settings.public.countdownseconds;
const TOTAL_QUESTIONS = Meteor.settings.public.totalquestions;

export default class App extends Component {

  constructor(props) {
    super(props);
    // oviously, we start we start button and thus is the quiz not started.
    this.state = {
      started: false,
      seconds: COUNTDOWN_IN_SECONDS,
      status: 'PRE_COUNTDOWN',
      question_count: 0,
      score: 0,
    };
    // bind clickhandler
    this.handleClick = this.handleClick.bind(this);
    this.tick = this.tick.bind(this);
    this.intervalHandle;
  }

  handleClick() {
    this.intervalHandle = setInterval(this.tick, 1000);
    this.setState(state => ({
      started: true,
      countdown: true,
      log: 2
    }));
  }

  tick() {
    if(this.state.question_count == TOTAL_QUESTIONS){
      this.setState({
        status: 'FINISHED'
      })      
    }

    else if(this.state.seconds > 1){
      var sec = this.state.seconds - 1;
      this.setState({
        seconds: sec
      })
    }

    else{
      if(this.state.status == 'PRE_COUNTDOWN'){
        this.setState({
          status: 'QUESTION_ACTIVE',
          seconds: TIMER_IN_SECONDS,
          question_count: this.state.question_count + 1
        })    
      } 
      else if(this.state.status == 'QUESTION_ACTIVE'){
        this.setState({
          status: 'PRE_COUNTDOWN',
          seconds: COUNTDOWN_IN_SECONDS
        })        
      }   
      this.forceUpdate();  
    }
  }



  render() {
    console.log(this.state.question_count)
    // if not started, 
    if(!this.state.started){
      return (
        <div className="container">
          <StartButton 
            onClick={this.handleClick}
          />
        </div>
      )
    }

    else if(this.state.status == 'FINISHED'){
      return(
          <div className="container">
            <div className="finishedwrap btn btn-start">
              FINISHED!
            </div>
          </div>
        )
    }

    return (
      <div className="container">
        <div className="presenter"></div>
        <div className="questionwrapper">
          <Timer seconds={this.state.seconds} />
          <QuestionContainer 
            status={this.state.status} 
            question_count={this.state.question_count} 
            />
        </div>
      </div>
    );
  }
}
